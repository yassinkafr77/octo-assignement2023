package ma.octo.assignement.repository;

import ma.octo.assignement.domain.*;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.domain.util.Gender;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;



@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class AuditTransferRepositoryTest {

    @Autowired
    AuditTransferRepository auditTransferRepository;



    @AfterEach
    void treatDown(){
        auditTransferRepository.deleteAll();
    }

    @Test
    public void checkExitingAuditById()  {


        //given
        AuditTransfer auditTransfer = new AuditTransfer();
        auditTransfer.setMessage("message test");
        AuditTransfer saved = auditTransferRepository.save(auditTransfer);

        //when
        AuditTransfer FindOne = auditTransferRepository.findById(saved.getId()).orElse(null);

       //then
        assertEquals(FindOne.getId(), saved.getId());


    }

    @Test
    public void checkNonExitingAuditById()  {
        //given
        AuditTransfer auditTransfer = new AuditTransfer();
        auditTransfer.setMessage("message test");
        auditTransfer.setEventType(EventType.DEPOSIT);
        AuditTransfer saved = auditTransferRepository.save(auditTransfer);

        //when
        AuditTransfer FindOne = auditTransferRepository.findById(saved.getId()+1).orElse(null);


        //then
        assertNull(FindOne);

    }

    @Test
    public void checkAllSavedAudits()  {
        ArrayList<AuditTransfer> all =  (ArrayList<AuditTransfer>) auditTransferRepository.findAll();
        assertEquals(all.size(),0);
    }

    @Test
    public void checkSavingInDataBase(){
        //given
        AuditTransfer auditTransfer = new AuditTransfer();
        auditTransfer.setMessage("message");

        //when
        AuditTransfer saved = auditTransferRepository.save(auditTransfer);

        //then
        assertSame(auditTransfer, saved);
    }

    @Test
    public void checkDeletingDataBase(){
        //given
        AuditTransfer auditTransfer = new AuditTransfer();
        auditTransfer.setMessage("message");
        AuditTransfer saved = auditTransferRepository.save(auditTransfer);
        auditTransferRepository.delete(saved);

        //when
        AuditTransfer found = auditTransferRepository.findById(saved.getId()).orElse(null);


        //then
        assertNull(found);
    }

}