package ma.octo.assignement.dto;

import lombok.*;
import ma.octo.assignement.domain.util.Gender;


import java.util.Date;



@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class UtilisateurDto {

    private Long id;

    private String username;

    private Gender gender;

    private String lastname;

    private String firstname;

    private Date birthdate;


}
