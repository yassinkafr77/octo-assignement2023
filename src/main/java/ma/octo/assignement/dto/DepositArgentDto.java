package ma.octo.assignement.dto;

import lombok.*;


import java.math.BigDecimal;
import java.util.Date;


@Data
@ToString
public class DepositArgentDto {

    private Long id;

    private BigDecimal montant;


    private Date dateExecution;


    private String nomPrenomEmetteur;


    private CompteDto compteBeneficiaire;


    private String motifDeposit;

    public DepositArgentDto(){

    }




}
