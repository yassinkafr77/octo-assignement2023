package ma.octo.assignement.web;


import lombok.extern.slf4j.Slf4j;

import ma.octo.assignement.dto.DepositArgentDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.MontantNonAutoriserException;
import ma.octo.assignement.exceptions.TransactionException;


import ma.octo.assignement.service.IAuditService;
import ma.octo.assignement.service.ICompteService;
import ma.octo.assignement.service.IDepositArgentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RequestMapping("/api/v1/depositArgent")
@RestController
@Slf4j
public class DepositArgentController {


    @Autowired
    private IDepositArgentService depositArgentService;



    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void depositArgent(@RequestBody DepositArgentDto depositArgentDto)
            throws CompteNonExistantException, TransactionException, MontantNonAutoriserException {
        log.debug("deposit Argent ...");
        depositArgentService.depositArgent(depositArgentDto);

    }



    @GetMapping
    List<DepositArgentDto> findAllDeposit() {

        log.debug("Lister les Deposits");
        return depositArgentService.findAll();
    }



}
