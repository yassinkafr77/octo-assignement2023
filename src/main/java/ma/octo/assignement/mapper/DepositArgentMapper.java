package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.DepositArgent;

import ma.octo.assignement.dto.DepositArgentDto;


public class DepositArgentMapper {



    public DepositArgentDto toDto(DepositArgent depositArgent) {
        if ( depositArgent == null ) {
            return null;
        }

        DepositArgentDto depositArgentDto = new DepositArgentDto();
        CompteMapper compteMapper = new CompteMapper();
        depositArgentDto.setId( depositArgent.getId() );
        depositArgentDto.setMontant( depositArgent.getMontant() );
        depositArgentDto.setDateExecution( depositArgent.getDateExecution() );
        depositArgentDto.setNomPrenomEmetteur( depositArgent.getNomPrenomEmetteur() );
        depositArgentDto.setCompteBeneficiaire( compteMapper.toDto( depositArgent.getCompteBeneficiaire() ) );
        depositArgentDto.setMotifDeposit( depositArgent.getMotifDeposit() );

        return depositArgentDto;
    }


    public DepositArgent toModel(DepositArgentDto depositArgentDto) {
        if ( depositArgentDto == null ) {
            return null;
        }

        DepositArgent depositArgent = new DepositArgent();
        CompteMapper compteMapper = new CompteMapper();
        depositArgent.setId( depositArgentDto.getId() );
        depositArgent.setMontant( depositArgentDto.getMontant() );
        depositArgent.setDateExecution( depositArgentDto.getDateExecution() );
        depositArgent.setNomPrenomEmetteur( depositArgentDto.getNomPrenomEmetteur() );
        depositArgent.setCompteBeneficiaire( compteMapper.toModel( depositArgentDto.getCompteBeneficiaire() ) );
        depositArgent.setMotifDeposit( depositArgentDto.getMotifDeposit() );

        return depositArgent;
    }



}
