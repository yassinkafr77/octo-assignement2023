package ma.octo.assignement.domain.util;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public enum EventType {

  TRANSFER("transfer"),
  DEPOSIT("Deposit d'argent");

  private String type;


}
