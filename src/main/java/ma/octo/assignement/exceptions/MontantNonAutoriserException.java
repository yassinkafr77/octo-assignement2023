package ma.octo.assignement.exceptions;

public class MontantNonAutoriserException extends Exception{


    public MontantNonAutoriserException() {
    }

    public MontantNonAutoriserException(String message) {
        super(message);
    }

    public MontantNonAutoriserException(String message, Throwable cause) {
        super(message, cause);
    }

    public MontantNonAutoriserException(Throwable cause) {
        super(cause);
    }
}
