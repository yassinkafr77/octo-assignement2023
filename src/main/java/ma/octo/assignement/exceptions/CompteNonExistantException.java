package ma.octo.assignement.exceptions;

public class CompteNonExistantException extends Exception {


  public CompteNonExistantException() {
  }

  public CompteNonExistantException(String message) {
    super(message);
  }

  public CompteNonExistantException(String message, Throwable cause) {
    super(message, cause);
  }

  public CompteNonExistantException(Throwable cause) {
    super(cause);
  }

  public CompteNonExistantException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

}
