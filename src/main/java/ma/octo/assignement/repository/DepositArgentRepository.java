package ma.octo.assignement.repository;

import ma.octo.assignement.domain.DepositArgent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepositArgentRepository extends JpaRepository<DepositArgent,Long> {


}
