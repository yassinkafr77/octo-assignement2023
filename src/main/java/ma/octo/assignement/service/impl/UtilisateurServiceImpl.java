package ma.octo.assignement.service.impl;

import lombok.AllArgsConstructor;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.mapper.UtilisateurMapper;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.service.IUtilisateurService;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;


@Service
@AllArgsConstructor
@Transactional
public class UtilisateurServiceImpl implements IUtilisateurService {


    private UtilisateurRepository utilisateurRepository;

    private final UtilisateurMapper utilisateurMapper = new UtilisateurMapper();







    @Override
    public List<UtilisateurDto> findAll(){
        List<Utilisateur> utilisateurList = utilisateurRepository.findAll();
        if (!CollectionUtils.isEmpty(utilisateurList)){
            List<UtilisateurDto> utilisateurDtos = new ArrayList<>();
            utilisateurList.forEach(utilisateur -> utilisateurDtos.add( utilisateurMapper.toDto(utilisateur)));

            return utilisateurDtos;
        }

        return null;
    }


    @Override
    public UtilisateurDto save(UtilisateurDto utilisateurDto) {

            if (utilisateurDto!=null){

                Utilisateur utilisateur = utilisateurMapper.toModel(utilisateurDto);
                Utilisateur savedUtilisateur =  utilisateurRepository.save(utilisateur);
                return  utilisateurMapper.toDto(savedUtilisateur);

            }
            return null;


    }


    @Override
    public UtilisateurDto findById(Long id) {
        Utilisateur utilisateur = utilisateurRepository.findById(id).orElse(null);

        if (utilisateur!=null){

            return utilisateurMapper.toDto(utilisateur);
        }

        return null;
    }
}
