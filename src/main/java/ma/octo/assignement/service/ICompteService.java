package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.CompteDto;


import java.util.List;

public interface ICompteService {

    List<CompteDto> findAll();

    CompteDto findByNumeroCompte(String numeroCompte);

    CompteDto save(CompteDto compte);

    CompteDto findByRib(String rib);



}
